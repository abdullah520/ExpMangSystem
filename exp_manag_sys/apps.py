# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class ExpManagSysConfig(AppConfig):
    name = 'exp_manag_sys'
