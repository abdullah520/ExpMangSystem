##                          Expense Management System

The project involves developing a website.The website has been developed to help people to manage their expenses in easy and feasible manner.In this era importance of management can not be denied.So idea of developing such project clicked in my mind.This projects would enable people to manage their expenses in much oragnized and estabished way.

###                              Getting Started

* To run this project its necessary to install virual environment.
* Open terminal in linux and type this command **sudo apt-get install virtualenv**
* Make a folder that contains a virtual environment e.g mkdir myfolder
* cd into selected folder
* Type python2 -m venv cvenv
* After setting up virtual environment install django
* python2 -m pip install --upgrade pip
* pip install -r requirements.txt
* To check if installed application type pip freeze

###                                Running Tests

* Type python manage.py test 
* To test coverage use **coverage --run include='./*' manage.py test**
* See coverage report **coverage html**


