from django.test import TestCase,Client
from django.urls import resolve
from django.urls import reverse
from ExpMangSystem.urls import urlpatterns
from ExpMangSystem.views import first_view,details
from ExpMangSystem.models import Item
from urllib import urlencode
from ExpMangSystem.forms import ItemDetail

class UrlsTest(TestCase):
   
    def test_first_view(self):
        response=self.client.get(reverse('first_view'))
        self.assertEqual(response.status_code, 200)
        
    
    def test_details(self):
        data={'itemId':5}
        response = self.client.get(reverse('details',kwargs=data))
        self.assertEqual(response.status_code, 200)
   

    def test_get_details(self):
        
        response = self.client.post('/ExpMangSystem/',{'name':'Pringles','price':100,'detail':'Chips'})
        self.assertEqual(response.status_code,200)
        response=self.client.get(reverse('get_details'))
        self.assertEqual(response.status_code, 200)

        
class EntryModelTest(TestCase):

    def test_string_representation(self):
        item = Item(item_name="django")
        self.assertEqual(str(item),  item.item_name)



