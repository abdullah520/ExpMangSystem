from __future__ import unicode_literals

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .models import Item
from .forms import ItemDetail


def first_view(request):
    list_items = Item.objects.order_by('buy_date')[:10]
    show = {'list_items': list_items}
    return render(request, 'first_view.html', show)


def details(request, itemId):
    return HttpResponse("You are looking at Item %s." % itemId)


def get_details(request):
    myform = ItemDetail(request.POST)
    if myform.is_valid():

        name = myform.cleaned_data['name']
        price = myform.cleaned_data['price']
        detail = myform.cleaned_data['detail']
        item = Item(
                    item_name=name,
                    item_price=price,
                    item_description=detail
                   )
        item.save()

        return render(
                      request, 'form1.html',
                      {'myform': myform, 'item': item}
                     )
    else:

        myform = ItemDetail()
        return render(request, 'form1.html', {'myform': myform})
