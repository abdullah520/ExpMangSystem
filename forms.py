from django import forms


class ItemDetail(forms.Form):

    name = forms.CharField(label='name', max_length=100)
    price = forms.CharField(label='price', max_length="100")
    detail = forms.CharField(label='detail', widget=forms.Textarea)
