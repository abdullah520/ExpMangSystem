from django.conf.urls import url
from ExpMangSystem import views

urlpatterns = [
    url(r'^ExpMangSystem/$', views.get_details, name='get_details'),
    url(r'^ExpMangSystem/first_view/$', views.first_view, name='first_view'),
    url(r'^ExpMangSystem/(?P<itemId>[0-9]+)/$', views.details, name='details'),

]

