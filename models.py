# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Item(models.Model):
    item_name = models.CharField(max_length=100)
    buy_date = models.DateTimeField(auto_now_add=True)
    item_description = models.CharField(max_length=500, default="Some text")
    item_price = models.IntegerField(default=0)

    def __str__(self):
        return self.item_name
